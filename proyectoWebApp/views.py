from django.shortcuts import render, HttpResponse

def home(request):
    vars = {}
    return render(request, "proyectoWebApp\home.html")

def services(request):
    vars = {}
    return render(request, "proyectoWebApp\services.html")

def store(request):
    vars = {}
    return render(request, "proyectoWebApp\store.html")

def blog(request):
    vars = {}
    return render(request, r"proyectoWebApp\blog.html")

def contact(request):
    vars = {}
    return render(request, "proyectoWebApp\contact.html")