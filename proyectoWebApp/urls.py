from django.urls import path
from proyectoWebApp import views

urlpatterns = [
    path('', views.home), #main route
    path('services/', views.services),
    path('store/', views.store),
    path('blog/', views.blog),
    path('contact/', views.contact)
]